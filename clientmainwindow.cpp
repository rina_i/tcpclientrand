#include "clientmainwindow.h"
#include "ui_clientmainwindow.h"

#include <QMessageBox>
#include <QThreadPool>

ClientMainWindow::ClientMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientMainWindow),
    connState(false),
    isError(false),
    errNum(0),
    tm (new QTimer())
{
    ui->setupUi(this);
    this->centralWidget()->setLayout(ui->layCentral);
    ui->gbRange->setLayout(ui->layRange);
    ui->gbServAddress->setLayout(ui->layServAddress);
    connect(ui->bStart, SIGNAL(clicked()),
            SLOT(startPressed()));

    lockInputs(false);
    connect (tm, &QTimer::timeout, [this](){
        lockInputs(connState);
        if (isError){
            errorOccurred(errNum);
            errNum = 0;
            isError = false;
        }
    });
    tm->start(10);

}

ClientMainWindow::~ClientMainWindow()
{
    tm->stop();
    delete ui;
}

void ClientMainWindow::lockInputs(bool isLock){
    ui->bStart->setEnabled(!isLock);
    ui->bStop->setEnabled(isLock);
    ui->gbRange->setEnabled(!isLock);
    ui->gbServAddress->setEnabled(!isLock);
}

void ClientMainWindow::startPressed(){
    //Проверка введенных данных
    if (!Auxiliary::isCorrectIp(ui->leIp->text())){
        QMessageBox::information(this, "Ошибка", "Введенный ip некорректен");
        return;
    }
    if (!Auxiliary::isCorrectRange(ui->sbMin->value(),
                                   ui->sbMax->value(),
                                   ui->sbStep->value())){
        QMessageBox::information(this, "Ошибка", "Указанный диапазон некорректен");
        return;
    }

    TcpState* st = new TcpState();
    ClientTcpWorker* worker = new ClientTcpWorker(st);

    worker->setAddress(ui->leIp->text(),
                       ui->sbPort->value());
    worker->setRange(ui->sbMin->value(), ui->sbMax->value());
    worker->setStep(ui->sbStep->value());

    connect(st, &TcpState::stateChanged,
            [this, st](int val)->void{
        connState = (val == TcpState::Connected);
        if (val == TcpState::Error){
            isError = true;
            errNum = st->getError();
        }
    });
    connect(st, SIGNAL(sended(double)), SLOT(numberSended(double)));
    connect(st, SIGNAL(recieved(double)), SLOT(numberRecieved(double)));
    connect(ui->bStop, &QPushButton::clicked, [this, worker](){
        worker->stop();
    });
    QThreadPool::globalInstance()->start(worker);
}

void ClientMainWindow::errorOccurred(int errNum){

    QString errString;
    switch (errNum){
    case TcpState::ConnectionFailed:{
        errString = "Не удалось соединиться с сервером";
    }break;
    case TcpState::WriteError:{
        errString = "Не удалось произвести запись";
    }break;
    case TcpState::NotNumber:{
        errString = "Полученный ответ не является числом";
    }break;
    case TcpState::TimeoutExpired:{
        errString = "Превышен интервал ожидания ответа";
    }break;
    case TcpState::ConnectionClosed:{
        errString = "Соединение закрыто сервером";
    }break;
    default: break;
    }
    QMessageBox::information(this, "Ошибка",
                             "Передача прервана. " + errString);
}

void ClientMainWindow::numberSended(double val){
    ui->leSended->setText(QString::number(val));
}

void ClientMainWindow::numberRecieved(double val){
    ui->leRecieved->setText(QString::number(val));
}

void ClientMainWindow::transmitEnded(){
    lockInputs(false);
}

