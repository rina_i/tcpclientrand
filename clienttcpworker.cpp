#include "clienttcpworker.h"

#include <QThread>
#include <QTime> //Для инициализации рандома

ClientTcpWorker::ClientTcpWorker(TcpState* st) :
    QRunnable(),
    isRun(false),
    sock(nullptr),
    min(0),
    max(10),
    step(1),
    state(st)
{

}

ClientTcpWorker::~ClientTcpWorker(){
    stop();
    QThread::currentThread()->msleep(1000);
    if (sock != 0
            && sock != nullptr){
        sock->disconnectFromHost();
        delete sock;
    }
    delete state;
}

void ClientTcpWorker::doWork(QTcpSocket *sock){
    sock->connectToHost(servIp, servPort);

    if (!sock->waitForConnected()){
        setError(TcpState::ConnectionFailed);
#ifdef DEBUG_TCP
        std::cout << "Socket not connected" << std::endl;
#endif
        return;
    }

    setState(TcpState::Connected);

    isRun = true;

#ifdef DEBUG_TCP
    std::cout << "Connected to host" << std::endl;
#endif
    qsrand(QTime::currentTime().msec());
    while (isRun && sock->state() == QAbstractSocket::ConnectedState){
        int len = (max - min)/step;
        double val = qrand()%(len)*step + min;
        std::string pack = QString::number(val).toStdString();
//Отправка пакета
        if (sock->write( pack.data(), pack.size() ) != pack.size()
                || !sock->waitForBytesWritten()){
            //Что-то пошло не так
#ifdef DEBUG_TCP
            std::cout << "Cant write, cause " << sock->errorString().toStdString() << std::endl;
#endif
            setError(TcpState::WriteError);
        }else{
            //Ok, записали
#ifdef DEBUG_TCP
            std::cout << "Bytes to write " << sock->bytesToWrite() << std::endl;
#endif
            //Оповещение о числе, которое записано
            sendNumber(val);
            //Ожидание ответа
            if (!sock->waitForReadyRead()){
                if (sock->state() != QAbstractSocket::ConnectedState)
                    setError(TcpState::ConnectionClosed);
                else{
                    setError(TcpState::TimeoutExpired);
                    sock->disconnectFromHost();
                }
                return;
            }

            if (sock->bytesAvailable() <= 0)
                continue;
            //Чтение ответа
            QByteArray arr = sock->read(sock->bytesAvailable());

            bool ok;
            val = arr.toDouble(&ok);
            if (!ok){
                setError(TcpState::NotNumber);
            }else{
                recvNumber(val);
            }
        }
        QThread::currentThread()->msleep(1000);
    }
    if (sock->state() != QTcpSocket::ConnectedState){
        setError(TcpState::ConnectionClosed);
    }else{
        sock->disconnectFromHost();
        setState(TcpState::Disconnected);
    }
}

void ClientTcpWorker::run(){
#ifdef DEBUG_TCP
    std::cout << "In start" << std::endl;
#endif
    sock = new QTcpSocket();
    const int maxBufSize = 1024;
    sock->setReadBufferSize(maxBufSize);
    sock->setSocketOption(QAbstractSocket::LowDelayOption, 1);

    doWork(sock);

    delete sock;
    sock = nullptr;

}

void ClientTcpWorker::stop(){
    isRun = false;
}




