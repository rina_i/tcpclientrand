#ifndef CLIENTMAINWINDOW_H
#define CLIENTMAINWINDOW_H

#include <QMainWindow>
#include <clienttcpworker.h>
#include <QThread>
#include <QTimer>

namespace Auxiliary{
/**
 * @brief isCorrectIp Проверяет, является ли строка
 * корректным IPv4 адресом
 * @param str передаваемый адрем
 * @return true, если строка является корректным ip адресом;
 * false иначе
 */
inline bool isCorrectIp(const QString& str){
    QStringList tabs = str.split(".");
    int size = tabs.size();
    if (size != 4)
        return false;
    for (int i = 0; i < size; ++i){
        bool ok;
        int val = tabs[i].toInt(&ok, 10);
        if (!ok || (val < 0) || (val > 255))
            return false;
    }
    return true;
}

/**
 * @brief isCorrectRange Проверк, корректен ли диапазон с шагом
 * @param min Минимальное значение
 * @param max Максимальное значение
 * @param step шаг
 * @return true, если диапазон корректен;
 * false иначе
 */
inline bool isCorrectRange(const double& min,
                           const double& max,
                           const double& step){
    //Шаг не должен быть отрицательным
    if (step < 0)
        return false;
    //Шаг не должен быть больше промежутка
    // (одновременно проверяем, что max > min)
    if ((max - min) < step)
        return false;
    return true;
}
}

namespace Ui {
class ClientMainWindow;
}

class ClientMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClientMainWindow(QWidget *parent = 0);
    ~ClientMainWindow();
private slots:
    void startPressed();
    void errorOccurred(int errNum);
    void numberSended(double val);
    void numberRecieved(double val);
    void transmitEnded();
private:
    Ui::ClientMainWindow *ui;
    volatile bool connState;
    volatile bool isError;
    int errNum;
    QTimer* tm;
    void lockInputs(bool isLock = true);
};

#endif // CLIENTMAINWINDOW_H
