#ifndef CLIENTTCPWORKER_H
#define CLIENTTCPWORKER_H

#include <QRunnable>
#include <QTcpSocket>
#include <tcpstate.h>


//#define DEBUG_TCP
#ifdef DEBUG_TCP
#include <iostream>
#endif

class ClientTcpWorker : public QRunnable
{

public:
    explicit ClientTcpWorker(TcpState* st);
    ~ClientTcpWorker();
    void setRange(const double& min, const double& max){
        this->max = max;
        this->min = min;
    }

    void setAddress(const QString& ip,
                    int port){
        this->servIp = ip;
        this->servPort = port;
    }

    void setStep(const double& step){
        this->step = step;
    }

    bool isRunning(){
        return ((sock != nullptr)
                && isRun);
    }

    void run() Q_DECL_OVERRIDE;

    void stop();

    void recvNumber(double val){
        if (state == 0 || state == nullptr)
            return;
        state->numRecieved(val);
    }

    void sendNumber(double val){
        if (state == 0 || state == nullptr)
            return;
        state->numSended(val);
    }

private:
    volatile bool isRun;
    QTcpSocket* sock;
    QString servIp;
    int servPort;
    double min;
    double max;
    double step;
    TcpState* state;

    void doWork(QTcpSocket* sock);

    void setState(TcpState::State st){
        if (state == 0
                || state == nullptr)
            return;
        state->setState(st);
    }
    void setError(TcpState::Errors err){
        if (state == 0
                || state == nullptr){
            return;
        }
        state->setError(err);
        state->setState(TcpState::Error);
    }
};

#endif // CLIENTTCPWORKER_H
