#-------------------------------------------------
#
# Project created by QtCreator 2015-04-25T14:11:38
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -std=c++11

TARGET = TcpClient
TEMPLATE = app


SOURCES += main.cpp\
        clientmainwindow.cpp \
    clienttcpworker.cpp \
    tcpstate.cpp

HEADERS  += clientmainwindow.h \
    clienttcpworker.h \
    tcpstate.h

FORMS    += clientmainwindow.ui
