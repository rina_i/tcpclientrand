#ifndef TCPSTATE_H
#define TCPSTATE_H

#include <QObject>

class TcpState : public QObject
{
    Q_OBJECT
public:
    enum State{
        Connected = 0,
        Disconnected,
        Error
    };
    enum Errors{
        NoError,
        ConnectionFailed,
        TimeoutExpired,
        ConnectionClosed,
        NotNumber,
        WriteError
    };

    explicit TcpState(QObject *parent = 0);
    void setState(State state){
        if (this->state != state){
            this->state = state;
            emit stateChanged(state);
        }
    }
    void setError(Errors errNum){
        this->errNum = errNum;
    }
    int getError() const{
        return errNum;
    }

    void numSended(double val){
        emit sended(val);
    }

    void numRecieved(double val){
        emit recieved(val);
    }

signals:
    void stateChanged(int state);
    void recieved(double val);
    void sended(double val);
private:
    int errNum;
    int state;    
};

#endif // TCPSTATE_H
